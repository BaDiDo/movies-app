# Welcome to your Expo app 👋

This is an [Expo](https://expo.dev) project created with [`create-expo-app`](https://www.npmjs.com/package/create-expo-app).

## Get started

1. Install dependencies

   ```bash
   npm install
   ```

   or

   ```bash
   yarn install
   ```

2. Start the app

   ```bash
    npx expo start
   ```

## Get a fresh project

When you're ready, run:

```bash
npm run reset-project
```

This command will move the starter code to the **app-example** directory and create a blank **app** directory where you can start developing.

## Packages used
- **tailwindcss 3.3.2** for easy use of **style classes**.
- **Formik: ^2.4.6** better **control** of inputs.
- **yup ^1.4.0**, better **validation** of inputs.
- **@reduxjs/toolkit ^2.2.4** for easy use of **API**.