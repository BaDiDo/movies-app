import { getApp, getApps, initializeApp } from "firebase/app";
import { getAuth, initializeAuth} from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyA7Gtgui1-ak6TYntfGnQFTjtoTo7E_Qc4",
    authDomain: "movies-app-5bd97.firebaseapp.com",
    projectId: "movies-app-5bd97",
    storageBucket: "movies-app-5bd97.appspot.com",
    messagingSenderId: "774462200388",
    appId: "1:774462200388:android:24edfa6b6d91f298283a11",
    measurementId: "G-V02RQLR654"
};

let app
let auth

if (getApps().length < 1) {
    app = initializeApp(firebaseConfig);
    auth = initializeAuth(app);
} else {
    app = getApp();
    auth = getAuth();
}

export { app, auth }