import React, { createContext, useContext, useEffect, useState } from "react";
import {useDispatch, useSelector} from "react-redux";
import {selectProfile, selectToken, setProfile} from "@/app/_redux/auth/authSlice";

const GlobalContext = createContext(false);
export const useGlobalContext = () => useContext(GlobalContext);

const GlobalProvider = ({ children }) => {

    const profile = useSelector(selectProfile)
    const [user, setUser] = useState(profile);
    const [isLogged, setIsLogged] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (profile) {
            setIsLogged(true);
        } else {
            setIsLogged(false);
        }
        setLoading(false)
    }, []);

    return (
        <GlobalContext.Provider value={{ isLogged, setIsLogged, user, setUser, loading }}>
            { children }
        </GlobalContext.Provider>
    );
};

export default GlobalProvider;