import logo from "../assets/images/icon.png"
import background from "../assets/images/posters-background.jpg"

export default {
    logo,
    background
}