import {Text, View} from 'react-native';
import {useSession} from "@/providers/AuthProvider";
import {useDispatch, useSelector} from "react-redux";
import {logOut as out, selectProfile} from "@/app/_redux/auth/authSlice";
import {signOut as firebaseSignOut} from '@firebase/auth';
import {auth} from "@/app/_layout";
import {router} from "expo-router";
import {StatusBar} from "expo-status-bar";


export default function Index() {

    const dispatch = useDispatch()
    const { signOut } = useSession();

    const profile = useSelector(selectProfile)

    const logOut = async () => {
        await firebaseSignOut(auth).then(() => {
            dispatch(out())
            signOut()
            router.replace('/');
        })
    }

    return (
        <>
            <StatusBar style="light" />
            <View className="w-full h-full flex flex-col justify-between items-center p-3">
                <Text className="text-white font-pblack text-xl">
                    Bonjour, { profile?.user.email }
                </Text>
                <Text
                    className="text-red-500"
                    onPress={logOut}>
                    Deconnexion
                </Text>
            </View>
        </>
    );

}
