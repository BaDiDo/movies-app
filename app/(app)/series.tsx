import {FlatList, ScrollView, Text, View} from "react-native";
import Poster from "@/components/Poster";
import {useGetTvShowsListQuery} from "@/app/_redux/movies/services";
import TopRatedTvShowsSlider from "@/components/TopRatedTvShowsSlider";
import CustomTitle from "@/components/CustomTitle";
import Pagination from "@/components/Paginations";
import {useState} from "react";

const Series = () => {

    const initialParams = { page: 1 }
    const [params, setParams] = useState(initialParams);
    const { data } = useGetTvShowsListQuery(params)
    const {results: tvShows, "total_pages": pages} = data || {}

    const renderItem = ({item}: {item: any}) =>  <Poster type="tv" key={item.id} show={item}/>

    return (
        <ScrollView className=" w-full h-screen">

            <View className="w-full h-full flex flex-col justify-between items-center gap-y-6">

                <View className="w-full flex flex-col justify-center items-start p-3">
                    <CustomTitle title="Meilleurs notés" />
                </View>

                <View className="w-full flex flex-col justify-center items-start p-3">
                    <TopRatedTvShowsSlider />
                </View>

                <View className="w-full flex flex-col justify-center items-start p-3">
                    <CustomTitle title="Series" />
                </View>

                { tvShows?.length > 0 && <Pagination params={params} setParams={setParams} pages={pages} /> }

                <FlatList
                    data={tvShows}
                    renderItem={renderItem}
                    keyExtractor={item => item?.id}
                    ItemSeparatorComponent={() => <View className="h-[20px]" />}
                    ListEmptyComponent={() => <Text>Vide</Text>}
                    numColumns={2}
                />

                { tvShows?.length > 0 && <Pagination params={params} setParams={setParams} pages={pages} /> }

            </View>

        </ScrollView>
    )
}

export default Series
