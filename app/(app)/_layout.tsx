import {Redirect, Stack, Tabs} from 'expo-router';
import {useSession} from "@/providers/AuthProvider";
import Loader from "@/components/Loader";
import {FontAwesome} from "@expo/vector-icons";
import {GestureHandlerRootView} from "react-native-gesture-handler";
import {Drawer} from "expo-router/drawer";
import CustomSearchLink from "@/components/CustomSearchLink";
import {Text, View} from "react-native";

export default function AppLayout() {
    const { session, isLoading } = useSession();

    // You can keep the splash screen open, or render a loading screen like we do here.
    if (isLoading) {
        return <Loader isLoading={isLoading} />
    }

    // Only require authentication within the (app) group's layout as users
    // need to be able to access the (auth) group and sign in again.
    if (!session) {
        // On web, static rendering will stop here as the user is not authenticated
        // in the headless Node process that the pages are rendered in.
        return <Redirect href="/sign-in" />;
    }

    // This layout can be deferred because it's not the root layout.
    return (
        <GestureHandlerRootView style={{ flex: 1 }}>
            <Drawer
                screenOptions={ {
                    headerTitleAlign: "center",
                    headerTintColor: "white",
                } }
            >
                <Drawer.Screen
                    name="index" // This is the name of the page and must match the url from root
                    options={{
                        drawerLabel: 'Acceuil',
                        title: 'Acceuil',
                    }}
                />
                <Drawer.Screen
                    name="movies" // This is the name of the page and must match the url from root
                    options={{
                        drawerLabel: 'Films',
                        title: 'Films',
                        headerRight: (props) => <CustomSearchLink type="movie" pathname="/search" otherProps={props} />
                    }}
                />
                <Drawer.Screen
                    name="series" // This is the name of the page and must match the url from root
                    options={{
                        drawerLabel: 'Series',
                        title: 'Series',
                        headerRight: (props) => <CustomSearchLink type="tv" pathname="/search" otherProps={props} />
                    }}
                />
            </Drawer>
        </GestureHandlerRootView>
    );
}
