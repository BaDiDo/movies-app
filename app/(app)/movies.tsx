import {FlatList, ScrollView, Text, View} from "react-native";
import {useGetMoviesListQuery} from "@/app/_redux/movies/services";
import Poster from "@/components/Poster";
import TopRatedMoviesSlider from "@/components/TopRatedMoviesSlider";
import CustomTitle from "@/components/CustomTitle";
import Pagination from "@/components/Paginations";
import {useState} from "react";

const Movies = () => {

    const initialParams = { page: 1 }
    const [params, setParams] = useState(initialParams);
    const { data } = useGetMoviesListQuery(params)
    const {results: movies, "total_pages": pages} = data || {}

    const renderItem = ({item}: {item: any}) => {
        return <Poster type="movie" key={item.id} show={item} />;
    };

    return (
        <ScrollView className=" w-full h-screen">

            <View className="w-full h-full flex flex-col justify-between items-center gap-y-6">

                <View className="w-full flex flex-col justify-center items-start p-3">
                    <CustomTitle title="Meilleurs notés" />
                </View>

                <View>
                    <TopRatedMoviesSlider />
                </View>

                <View className="w-full flex flex-col justify-center items-start p-3">
                    <CustomTitle title="Films" />
                </View>

                { movies?.length > 0 && <Pagination params={params} setParams={setParams} pages={pages} /> }

                <FlatList
                    data={movies}
                    renderItem={renderItem}
                    keyExtractor={item => item?.id}
                    ItemSeparatorComponent={() => <View className="h-[20px]" />}
                    ListEmptyComponent={() => <Text>Vide</Text>}
                    numColumns={2}
                />

                { movies?.length > 0 && <Pagination params={params} setParams={setParams} pages={pages} /> }

            </View>

        </ScrollView>
    )
}

export default Movies
