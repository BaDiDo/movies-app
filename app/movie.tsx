import {ScrollView, Text, View} from "react-native";
import {useGetMovieItemQuery} from "@/app/_redux/movies/services";
import {Image, ImageBackground} from "expo-image";
import {useLocalSearchParams} from "expo-router";
import Loader from "@/components/Loader";
import Trailers from "@/components/Trailers";
import {Header} from "@react-navigation/elements";

const Movie = () => {

    const { id } = useLocalSearchParams();

    const { data, isLoading } = useGetMovieItemQuery({ id: id })

    return (
        !isLoading ?
                <ImageBackground
                    className="w-full flex flex-col justify-start items-center"
                    source={"https://image.tmdb.org/t/p/w600_and_h900_bestv2" + data["backdrop_path"]}
                >
                    <Header
                        title={data.title} headerTransparent={true}
                        headerRight={(props) => <Text {...props}>dasdas</Text>}
                    />
                    <View
                        className="w-full h-full"
                    >
                        <ScrollView>
                                <View
                                    className="w-full flex flex-col justify-start items-start py-9"
                                >
                                    <View className="h-full w-full flex flex-col justify-start items-center">
                                        <Image
                                            className="w-[200px] h-[200px] mt-6 rounded-3xl"
                                            source={"https://image.tmdb.org/t/p/w600_and_h900_bestv2" + data["poster_path"]}
                                        />
                                        <Text className="text-white font-pblack text-ellipsis text-2xl">
                                            { data.name }
                                        </Text>
                                        <View className="flex flex-col justify-end  text-white font-pregular text-justify rounded-sm">
                                            <Text className="text-white font-pblack text-xl text-justify p-3">
                                                Resumé
                                            </Text>
                                            <Text className="text-white font-pextralight text-md text-justify p-3">
                                                { data.overview }
                                            </Text>
                                            <Trailers id={data.id} />
                                        </View>
                                    </View>
                                </View>
                        </ScrollView>
                    </View>
                </ImageBackground>
            :
            <Loader isLoading={isLoading} />
    )
}

export default Movie