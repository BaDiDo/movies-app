import {useDispatch} from "react-redux";
import {Image, Text, ToastAndroid, View} from "react-native";
import images from "@/constants/Images";
import {object, string} from "yup";
import {useEffect, useState} from "react";
import {onAuthStateChanged, signInWithEmailAndPassword} from '@firebase/auth';
import {logIn, setProfile} from "@/app/_redux/auth/authSlice";
import {ImageBackground} from "expo-image";
import {Link, router} from "expo-router";
import {auth} from "@/app/_layout";
import {useSession} from "@/providers/AuthProvider";
import {Formik} from "formik";
import {StatusBar} from "expo-status-bar";
import FormikInput from "@/components/CustomField";
import CustomSubmitButton from "@/components/CustomSubmitButton";

export  default function SignIn () {

    const dispatch = useDispatch()
    const { signIn } = useSession()
    const [isLoading, setIsLoading] = useState(false)

    const loginSchema = object().shape({
        email: string().email("E-mail incorrecte").required("E-mail requis"),
        password: string().required("Minimum 6 caractères").min(6),
    });

    let initialValues = {
        email: "",
        password: "",
    }

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) dispatch(setProfile(user))
        });
        return () => unsubscribe();
    }, []);

    const handleAuthentication = async (values, formikHelpers) => {

        formikHelpers.setSubmitting(true)
        await signInWithEmailAndPassword(auth, values.email, values.password).then((userCredential) => {
            if (userCredential) {
                dispatch(setProfile(userCredential))
                dispatch(logIn({ user: userCredential, token: userCredential.user.refreshToken }))
                signIn()
                router.replace('/');
            }
        }).catch((error) => {
            let message = undefined
            if(error.code === "auth/invalid-email") {
                message = "Email invalid"
            } else if(error.code === "auth/invalid-credential") {
                message = "Email ou mot de passe invalid"
            } else {
                message = "Une erreur est survenu veillez reessyez plutard"
            }
            ToastAndroid.show(message, ToastAndroid.BOTTOM)
            formikHelpers.setSubmitting(false)
        }).finally(() => formikHelpers.setSubmitting(false));

    }

    return (
        <View className="w-full h-screen">
            <StatusBar style="light" />
            <ImageBackground source={images.background} contentFit="cover">
                <View
                    className="h-full p-4 flex flex-col justify-between items-center object-contain"
                    style={{ backgroundColor: "rgba(0,0,0,0.8)" }}
                >

                    <View className="w-full justify-center items-center">
                        <Image source={images.logo} className="h-1/2 w-screen object-cover" />
                    </View>

                    <Text className="font-pblack text-white text-xl">
                        Connexion
                    </Text>

                    <View className="w-full flex flex-col justify-between gap-6 items-stretch">

                        <View className="w-full">
                            <Formik
                                initialValues={initialValues}
                                onSubmit={(values, formikHelpers) => { handleAuthentication(values, formikHelpers) }}
                                validationSchema={loginSchema}
                            >
                                {({ handleChange, handleBlur, handleSubmit, values, errors, isSubmitting }) => (
                                    <View className="w-full flex flex-col justify-between gap-6 items-stretch">

                                        <View>
                                            <FormikInput
                                                secureTextEntry={false}
                                                placeholder="Email"
                                                error={errors.email}
                                                handleChange={handleChange('email')}
                                                handleBlur={handleBlur('email')}
                                                value={values.email}
                                            />
                                        </View>

                                        <View>
                                            <FormikInput
                                                placeholder="Mot de passe"
                                                error={errors.password}
                                                handleChange={handleChange('password')}
                                                handleBlur={handleBlur('password')}
                                                value={values.password}
                                                secureTextEntry={true}
                                            />
                                        </View>

                                        <View>
                                            <CustomSubmitButton
                                                isSubmitting={isSubmitting}
                                                handleSubmit={handleSubmit}
                                                title="Connexion"
                                            />
                                        </View>

                                    </View>
                                )}

                            </Formik>
                        </View>

                        <View className="w-full">
                            <Text className="text-white font-pregular p-3 text-justify">
                                Vous n'avez pas encore de compte,&nbsp; &nbsp;
                            <Link replace href="/sign-up" >
                                <Text className="text-white underline font-pbold p-3 text-justify">
                                     Inscrivez-vous
                                </Text>
                            </Link>
                            </Text>
                        </View>

                    </View>

                </View>

            </ImageBackground>
        </View>
    )
}
