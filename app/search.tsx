import {FlatList, SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native";
import {useSearchListQuery} from "@/app/_redux/movies/services";
import Poster from "@/components/Poster";
import {useState} from "react";
import {Appbar, Button} from "react-native-paper";
import Pagination from "@/components/Paginations";
import {useLocalSearchParams} from "expo-router";
import {StatusBar} from "expo-status-bar";

const Search = () => {

    const { type } = useLocalSearchParams();
    const initialParams = { query: "", page: 1 }
    const [params, setParams] = useState(initialParams);
    const { data, isLoading } = useSearchListQuery({ gender: type, params: params })
    const { results: items, "total_pages": pages } = data || {}

    const renderItem = ({item}: {item: any}) =>  <Poster type="tv" key={item.id} show={item}/>

    if (isLoading) {
        return <Text>Chargement...</Text>
    }

    return (
        <SafeAreaView className="bg-primary">
            <StatusBar style="light" />
            <Appbar className="p-3 bg-primary" mode="medium" collapsable={true}>
                <View className="w-full text-white p-3 bg-[#2a2a2a] border-2 border-[#2a2a2a] focus:border-white rounded-xl">
                    <TextInput
                        autoComplete="name"
                        placeholderTextColor="white"
                        className="text-white font-pregular"
                        value={params.query}
                        onChangeText={(text) => { setParams({ query: text , page: 1 }) }}
                        placeholder="Recherche"
                    />
                </View>
            </Appbar>

            { items.length > 0 && <Pagination params={params} setParams={setParams} pages={pages} /> }

            <View className="w-full h-full flex flex-col justify-between items-center gap-y-2">
                <FlatList
                    data={items}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                    stickyHeaderHiddenOnScroll={true}
                    ItemSeparatorComponent={() => <View className="h-[20px]" />}
                    ListEmptyComponent={() =>
                        <View className="w-screen h-screen flex flex-col justify-center items-center">
                            <Text className="text-white text-xl">0 Resultat</Text>
                        </View>
                    }

                />
            </View>
        </SafeAreaView>
    )
}

export default Search
