import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {Image, Text, ToastAndroid, TouchableOpacity, View} from "react-native";
import images from "@/constants/Images";
import {object, ref, string} from "yup";
import { createUserWithEmailAndPassword, onAuthStateChanged } from '@firebase/auth';
import {logIn, setProfile} from "@/app/_redux/auth/authSlice";
import {auth} from "@/app/_layout";
import {ImageBackground} from "expo-image";
import {Link} from "expo-router";
import {Formik} from "formik";
import FormikInput from "@/components/CustomField";

export default function SignUp () {

    const dispatch = useDispatch()

    const registerSchema = object().shape({
        email: string().
            email("E-mail incorrecte").
            required("E-mail requis"),
        password: string().
            required("Minimum 6 caractères").min(6),
        confirmation: string().
            required("Minimum 6 caractères").
            oneOf([ref('password')], 'Ne correspondent pqs')
    });

    let initialValues = {
        email: "",
        password: "",
        confirmation: "",
    }

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) dispatch(setProfile(user))
        });
        return () => unsubscribe();
    }, [auth]);

     async function handleAuthentication(values) {

        await createUserWithEmailAndPassword(auth, values.email, values.password).then((userCredential) => {
            if (userCredential) {
                dispatch(setProfile(userCredential))
                dispatch(logIn({ user: userCredential, token: userCredential.user.refreshToken }))
            }
        }).catch((error) => {
            let message = undefined
            if(error.code === "auth/invalid-email") {
                message = "Email invalid"
            } else if(error.code === "auth/invalid-credential") {
                message = "Email ou mot de passe invalid"
            } else {
                message = "Une erreur est survenu veillez reessyez plutard"
            }
            ToastAndroid.show(message, ToastAndroid.BOTTOM)
        });

    }

    return (
        <ImageBackground source={images.background} contentFit="cover">
            <View
                className="w-full h-full p-4 flex flex-col justify-between items-center object-contain"
                style={{ backgroundColor: "rgba(0,0,0,0.82)" }}
            >

                <View className="w-full justify-center items-center">
                    <Image source={images.logo} className="h-1/3 w-screen object-cover" />
                </View>

                <Text className="font-pblack text-white text-xl">
                    Inscription
                </Text>

                <View className="w-screen flex flex-col justify-center items-center gap-y-6 p-3">

                    <Formik
                        initialValues={initialValues}
                        onSubmit={handleAuthentication}
                        validationSchema={registerSchema}
                    >
                        {({ handleChange, handleBlur, handleSubmit, values, errors, isSubmitting }) => (
                            <View className="w-full flex flex-col justify-between gap-6 items-stretch">

                                <View>
                                    <FormikInput
                                        secureTextEntry={false}
                                        placeholder="Email"
                                        error={errors.email}
                                        handleChange={handleChange('email')}
                                        handleBlur={handleBlur('email')}
                                        value={values.email}
                                    />
                                </View>

                                <View>
                                    <FormikInput
                                        placeholder="Mot de passe"
                                        error={errors.password}
                                        handleChange={handleChange('password')}
                                        handleBlur={handleBlur('password')}
                                        value={values.password}
                                        secureTextEntry={true}
                                    />
                                </View>

                                <View>
                                    <FormikInput
                                        placeholder="Confirmation"
                                        error={errors.confirmation}
                                        handleChange={handleChange('confirmation')}
                                        handleBlur={handleBlur('confirmation')}
                                        value={values.confirmation}
                                        secureTextEntry={true}
                                    />
                                </View>

                                <View>
                                    <TouchableOpacity
                                        disabled={isSubmitting}
                                        className="w-full bg-cyan-500 bg-gradient-to-r from-cyan-500 to-blue-700 text-white rounded-xl"
                                        onPress={handleSubmit}
                                        click
                                    >
                                        <Text className="text-white font-pbold p-3 text-center">
                                            Inscription
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        )}


                    </Formik>

                    <View className="w-full">
                        <Text className="text-white font-pregular p-3 text-justify">
                            Vous avez déja un compte,&nbsp; &nbsp;
                            <Link replace href="/sign-in" >
                                <Text className="text-white underline font-pbold p-3 text-justify">
                                    Connectez-vous
                                </Text>
                            </Link>
                        </Text>
                    </View>

                </View>

            </View>

        </ImageBackground>
    )
}