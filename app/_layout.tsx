import {persistor, store} from "@/app/_redux/store";
import { DarkTheme, ThemeProvider } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import {Slot} from 'expo-router';
import * as SplashScreen from 'expo-splash-screen';
import { useEffect } from 'react';
import 'react-native-reanimated';

import { useColorScheme } from '@/hooks/useColorScheme';
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import {getApp, getApps, initializeApp} from "firebase/app";
import {initializeAuth, getReactNativePersistence, getAuth} from "firebase/auth";
import ReactNativeAsyncStorage from '@react-native-async-storage/async-storage';
import {PaperProvider} from "react-native-paper";
import {SessionProvider} from "@/providers/AuthProvider";
import {RootSiblingParent} from "react-native-root-siblings";

SplashScreen.preventAutoHideAsync();

const firebaseConfig = {
    apiKey: "AIzaSyA7Gtgui1-ak6TYntfGnQFTjtoTo7E_Qc4",
    authDomain: "movies-app-5bd97.firebaseapp.com",
    projectId: "movies-app-5bd97",
    storageBucket: "movies-app-5bd97.appspot.com",
    messagingSenderId: "774462200388",
    appId: "1:774462200388:android:24edfa6b6d91f298283a11",
    measurementId: "G-V02RQLR654"
};

let app
let auth

if(getApps().length <= 0) {
    app = initializeApp(firebaseConfig);
    auth = initializeAuth(app, {
        persistence: getReactNativePersistence(ReactNativeAsyncStorage)
    });
} else {
    app = getApp()
    auth = getAuth()
}

export { app, auth }



export default function Root() {

    const colorScheme = useColorScheme();
    const [loaded, error] = useFonts({
        "Poppins-Thin": require('../assets/fonts/Poppins/Poppins-Thin.ttf'),
        "Poppins-ThinItalic": require('../assets/fonts/Poppins/Poppins-ThinItalic.ttf'),
        "Poppins-ExtraLight": require('../assets/fonts/Poppins/Poppins-ExtraLight.ttf'),
        "Poppins-ExtraLightItalic": require('../assets/fonts/Poppins/Poppins-ExtraLightItalic.ttf'),
        "Poppins-Light": require('../assets/fonts/Poppins/Poppins-Light.ttf'),
        "Poppins-LightItalic": require('../assets/fonts/Poppins/Poppins-LightItalic.ttf'),
        "Poppins-Regular": require('../assets/fonts/Poppins/Poppins-Regular.ttf'),
        "Poppins-Italic": require('../assets/fonts/Poppins/Poppins-Italic.ttf'),
        "Poppins-SemiBold": require('../assets/fonts/Poppins/Poppins-SemiBold.ttf'),
        "Poppins-SemiBoldItalic": require('../assets/fonts/Poppins/Poppins-SemiBoldItalic.ttf'),
        "Poppins-Bold": require('../assets/fonts/Poppins/Poppins-Bold.ttf'),
        "Poppins-BoldItalic": require('../assets/fonts/Poppins/Poppins-BoldItalic.ttf'),
        "Poppins-ExtraBold": require('../assets/fonts/Poppins/Poppins-ExtraBold.ttf'),
        "Poppins-ExtraBoldItalic": require('../assets/fonts/Poppins/Poppins-ExtraBoldItalic.ttf'),
        "Poppins-Black": require('../assets/fonts/Poppins/Poppins-Black.ttf'),
        "Poppins-BlackItalic": require('../assets/fonts/Poppins/Poppins-BlackItalic.ttf')
    });

    useEffect(() => {
        if (error) throw error
        if (loaded) SplashScreen.hideAsync()
    }, [loaded, error]);

    if (!loaded && !error) {
        return null;
    }

    return (
        <RootSiblingParent>
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                        <PaperProvider>
                            <ThemeProvider value={DarkTheme}>
                                    <SessionProvider>
                                        <Slot />
                                    </SessionProvider>
                            </ThemeProvider>
                        </PaperProvider>
                </PersistGate>
            </Provider>
        </RootSiblingParent>
    );
}