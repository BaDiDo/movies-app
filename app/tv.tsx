import {ScrollView, Text, View} from "react-native";
import {useGetTvShowItemQuery} from "@/app/_redux/movies/services";
import {Image, ImageBackground} from "expo-image";
import {useLocalSearchParams} from "expo-router";
import Loader from "@/components/Loader";
import Trailers from "@/components/Trailers";
import {Header} from "@react-navigation/elements";

const Tv = () => {

    const { id } = useLocalSearchParams();

    const { data, isLoading } = useGetTvShowItemQuery({ id: id })

    return (
        !isLoading ?
                <>
                    <ImageBackground
                        className="w-full flex flex-col justify-start items-center"
                        source={"https://image.tmdb.org/t/p/w600_and_h900_bestv2" + data["backdrop_path"]}
                    >
                        <View
                            className="w-full h-full"
                            style={{ backgroundColor: "rgba(0,0,0,0.41)" }}
                        >
                            <ScrollView>
                                <Header title={ data.name } headerTransparent={true} headerTitleAlign="center"  />
                                    <View
                                        className="w-full flex flex-col justify-start items-start py-9"
                                    >
                                        <View className="h-full w-full flex flex-col justify-start items-center gap-y-2">
                                            <Image
                                                className="w-[200px] h-[200px] mt-6 rounded-3xl"
                                                source={"https://image.tmdb.org/t/p/w600_and_h900_bestv2" + data["poster_path"]}
                                            />
                                            <Text className="text-white font-pblack text-ellipsis text-2xl">
                                                { data.name }
                                            </Text>
                                            <View className="flex flex-col justify-end  text-white font-pregular text-justify p-1 rounded-sm">
                                                <Text className="text-white font-pblack text-xl text-justify p-3">
                                                    Resumé
                                                </Text>
                                                <Text className="text-white font-pextralight text-md text-justify p-3">
                                                    { data.overview }
                                                </Text>
                                                <Trailers id={data.id} />
                                            </View>
                                        </View>
                                    </View>
                            </ScrollView>
                        </View>
                    </ImageBackground>
                </>
            :
            <Loader isLoading={isLoading} />
    )
}

export default Tv