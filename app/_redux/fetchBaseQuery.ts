import {RootState} from "./store";
import querystring from "query-string";
import {urlHelpers} from "@/app/_redux/urlHelpers";
import {fetchBaseQuery} from "@reduxjs/toolkit/query";


export const globalFetchBaseQuery = fetchBaseQuery({
    baseUrl: "https://api.themoviedb.org/",
    paramsSerializer: (params: Record<string, any>) => querystring.stringify(params, {arrayFormat: 'bracket'}),
    prepareHeaders: (headers, api) => {

        headers.set('authorization', 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3YTlkZTA4MGQ4NzBhZDUwM2UxZjIyNzEyMDA1NzUwYSIsInN1YiI6IjY2M2M4Nzg4ZTFiYTNhN2U0YmZhM2E1MyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Tu3_M0fZv4D3vN6_roM9TcLsVAl8n-3ZcagRhKk7P1o')
        headers.set('accept-Language', "fr");

        return headers
    },
})

export function providesList<R extends { id: string | number }[], T extends string>(
    resultsWithIds: R | undefined,
    tagType: T
) {

    if (typeof resultsWithIds == "undefined") {
        return [];
    }

    if (Array?.isArray(resultsWithIds)) {
        return [
            { type: tagType, id: 'LIST' },
            ...resultsWithIds?.map(({ id }) => ({ type: tagType, id })),
        ];
    }

    if ("list" in resultsWithIds) {
        return [
            { type: tagType, id: 'LIST' },
            ...resultsWithIds?.list?.map(({ id }) => ({ type: tagType, id })),
        ];
    }

    return [{ type: tagType, id: 'LIST' }];
}



export function invalidatesList(
    tagTypes: string[],
    id: number | string | undefined,
) {

    return [
        ...tagTypes?.map(tagType => ({type: tagType, id})),
    ];
}

export const TransformListResponse = (response: any, meta: any, arg: any) => {

    const pagination = "hydra:view" in response ? response["hydra:view"] : {};
    const first = "hydra:first" in pagination ? pagination["hydra:first"] : "";
    const last = "hydra:last" in pagination ? pagination["hydra:last"] : "";
    const next = "hydra:next" in pagination ? pagination["hydra:next"] : "";
    const current = "@id" in pagination ? pagination["@id"] : "";

    return {
        list: "hydra:member" in response ? response["hydra:member"] : [],
        pagination: {
            first: urlHelpers(first),
            last: urlHelpers(last),
            next: urlHelpers(next),
            current: urlHelpers(current),
        },
        totalItems: "hydra:totalItems" in response ? response["hydra:totalItems"] : 0,
    }
}

export const TransformItemResponse = (response: any, meta: any, arg: any) => {
    return {
        item: response,
    }
}