import {
    combineReducers,
    configureStore,
    ThunkDispatch,
} from '@reduxjs/toolkit';
import authReducer from "./auth/authSlice";
import moviesReducer from "./movies/moviesSlice";
import {persistReducer, persistStore} from 'redux-persist';
import {useDispatch} from "react-redux";
import {setupListeners} from "@reduxjs/toolkit/query";
import {moviesApi} from "@/app/_redux/movies/services";
import logger from "redux-logger";
import AsyncStorage from "@react-native-async-storage/async-storage";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
};

const reducers = combineReducers({
    auth: authReducer,
    movies: moviesReducer,
    [moviesApi.reducerPath]: moviesApi.reducer,
})

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({ serializableCheck: false, immutableCheck: false })
        .concat(moviesApi.middleware),
    devTools: process.env.NODE_ENV !== 'production',
})

export const useAppDispatch = () => useDispatch<ThunkDispatch<any, any, any>>()
setupListeners(store.dispatch)

export const persistor = persistStore(store)
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;