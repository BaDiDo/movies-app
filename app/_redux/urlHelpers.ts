export function urlHelpers(uri: string) {
    const url = new URL(process.env.REACT_APP_API_BASE_URL + uri);
    const params = url.searchParams;
    const paramsObject = {};

    for (const [key, value] of params.entries()) {
        paramsObject[key] = value;
    }

    return paramsObject
}
export function filterObjToString(filters: object): string {

    const params = new URLSearchParams();

    Object.keys(filters).forEach(key => {

        if (filters[key] !== "" && filters[key] !== undefined ) {

            params.append(key, filters[key])
        }
    })

    return params.toString()
}