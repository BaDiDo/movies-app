export interface AuthState {
    user: object|undefined,
    token: string|undefined,
}