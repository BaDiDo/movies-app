import {createSlice} from '@reduxjs/toolkit'
import axios from "axios";
import {RootState} from "../store";
import storage from "redux-persist/lib/storage";
import {AuthState} from "@/app/_redux/auth/types";

const initialState: AuthState = {
    user: undefined,
    token: undefined,
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        logIn: (state, action) => {
            const { token, user } = action.payload;
            return { ...state, token: token, user: user };
        },
        logOut: () => {
            return {
                user: undefined,
                token: undefined,
            };
        },
        setProfile: (state, action) => {

            const { user } = action.payload;
            return { ...state, user: user };

        }
    },
})

export const selectProfile = (state: RootState) => state.auth.user;
export const selectToken = (state: RootState) => state.auth.token;

export const {
    logIn,
    logOut,
    setProfile,
} = authSlice.actions

// Other code such as selectors can use the imported `RootState` type

export default authSlice.reducer