export interface MovieInterface {
    title: string,
    image: string,
}

export interface GenderInterface {
    id: number,
    name: string
}
