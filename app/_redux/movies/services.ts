import {createApi} from '@reduxjs/toolkit/query/react'
import {globalFetchBaseQuery} from "./../fetchBaseQuery";

export const moviesApi = createApi({
    reducerPath: 'moviesApi',
    tagTypes: ["movies", "movie", "tvShows", "tvShow", "movieVideos", "tvShowVideos", "topRatedMovies", "topRatedTvShows", "genders"],
    baseQuery: globalFetchBaseQuery,
    endpoints: (builder) => ({
        searchList: builder.query<any, object|undefined>({
            query: (filters: object = { gender: "tv", params: { page: 1, language: "fr-FR" } }) => {
                // return `api/params_formation_types?${filterObjToString(filters)}`
                return {
                    url : `3/search/${filters.gender}`,
                    params : filters?.params
                }
            },
            providesTags: ["movies", "tvShows"],
        }),
        getTvShowVideos: builder.query<any, number|undefined>({
            query: (id: number|undefined) => {
                return {
                    url : `3/tv/${id}/videos`,
                }
            },
            providesTags: ["tvShowVideos"],
        }),
        getMovieVideos: builder.query<any, number|undefined>({
            query: (id: number|undefined) => {
                return {
                    url : `3/movie/${id}/videos`,
                }
            },
            providesTags: ["movieVideos"],
        }),
        searchMoviesList: builder.query<any, object|undefined>({
            query: (filters?: object) => {
                return {
                    url : '3/search/movie',
                    params : filters
                }
            },
            providesTags: ["movies"],
        }),
        getMoviesList: builder.query<any, object|undefined>({
            query: (filters: object = {page: 1}) => {
                return {
                    url : '3/movie/popular',
                    params : filters
                }
            },
            providesTags: ["movies"],
        }),
        getMovieItem: builder.query<any, object|undefined>({
            query: (filter: object|undefined = { id: undefined, gender: "tv" }) => {
                return {
                    url : `3/movie/${filter?.id}`,
                }
            },
            providesTags: ["movie"],
        }),
        searchTvShowsList: builder.query<any, object|undefined>({
            query: (filters: object = { page: 1 }) => {
                return {
                    url : '3/search/tv',
                    params : filters
                }
            },
            providesTags: ["tvShows"],
        }),
        getTvShowsList: builder.query<any, object|undefined>({
            query: (filters: object = {page: 1}) => {
                return {
                    url : '3/tv/popular',
                    params: filters
                }
            },
            providesTags: ["tvShows"],
        }),
        getTvShowItem: builder.query<any, object|undefined>({
            query: (filter: object|undefined = { id: undefined}) => {
                return {
                    url : `3/tv/${filter?.id}`,
                }
            },
            providesTags: ["tvShow"],
        }),
        getTopRatedMovies: builder.query({
            query: () => {
                return {
                    url : `3/movie/top_rated`,
                }
            },
            providesTags: ["topRatedMovies"],
        }),
        getTopRatedTvShows: builder.query({
            query: () => {
                return {
                    url : `3/tv/top_rated`,
                }
            },
            providesTags: ["topRatedTvShows"],
        }),
        getGenders: builder.query<any[], string>({
            query: (gender: string = "movie") => {
                return {
                    url : `3/genre/${gender}/list`,
                }
            },
            providesTags: ["genders"],
        }),
    })

})




// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
    useSearchListQuery,
    useGetTvShowVideosQuery,
    useGetMovieVideosQuery,
    useSearchMoviesListQuery,
    useGetMoviesListQuery,
    useGetMovieItemQuery,
    useGetTvShowsListQuery,
    useSearchTvShowsListQuery,
    useGetTvShowItemQuery,
    useGetTopRatedMoviesQuery,
    useGetTopRatedTvShowsQuery,
    useGetGendersQuery,
} = moviesApi

