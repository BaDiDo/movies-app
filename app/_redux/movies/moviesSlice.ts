import {createSlice} from '@reduxjs/toolkit';
import {RootState} from "./../store";

export interface SearchInterface {
    query?: string,
    "include_adult"?: boolean,
    language?: string | undefined,
    page: number
}

export interface MoviesState {
    search: SearchInterface,
    pages: number
}

const initialState: MoviesState = {
    search: {
        query: "super girl",
        "include_adult": true,
        language: "en-US",
        page: 1
    },
    pages: 1
}

export const movies = createSlice({
    name: 'movies',
    initialState,
    reducers: {
        setSearch: (state, action) => {
            const {search} = action.payload
            return { ...state, search };
        },
        setPages: (state, action) => {
            const {pages} = action.payload
            return { ...state, pages };
        }
    },
})

export const search = (state: RootState) => state.movies.search;
export const pages = (state: RootState) => state.movies.pages;

export const { setSearch } = movies.actions;

export default movies.reducer;