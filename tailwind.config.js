/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
      "./app/**/*.{js,jsx,ts,tsx}",
      "./components/**/*.{js,jsx,ts,tsx}",
      "./constants/**/*.{js,jsx,ts,tsx}",
      "./hooks/**/*.{js,jsx,ts,tsx}",
      "./assets/**/*.{js,jsx,ts,tsx}",
      "./App.{js,jsx,ts,tsx}",
      "./screens/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {
    extend: {
        colors: {
            primary: "#181818"
        },
        fontFamily: {
            pthin: ["Poppins-Thin", "sans-serif"] ,
            pthinitalic: ["Poppins-ThinItalic", "sans-serif"] ,
            pextralight: ["Poppins-ExtraLight", "sans-serif"] ,
            pextralightitalic: ["Poppins-ExtraLightItalic", "sans-serif"] ,
            plight: ["Poppins-Light", "sans-serif"] ,
            plightitalic: ["Poppins-LightItalic", "sans-serif"] ,
            pregular: ["Poppins-Regular", "sans-serif"] ,
            pitalic: ["Poppins-Italic", "sans-serif"] ,
            psemibold: ["Poppins-SemiBold", "sans-serif"] ,
            psemibolditalic: ["Poppins-SemiBoldItalic", "sans-serif"] ,
            pbold: ["Poppins-Bold", "sans-serif"] ,
            pbolditalic: ["Poppins-BoldItalic", "sans-serif"] ,
            pextrabold: ["Poppins-ExtraBold", "sans-serif"] ,
            pextrabolditalic: ["Poppins-ExtraBoldItalic", "sans-serif"] ,
            pblack: ["Poppins-Black", "sans-serif"] ,
            pblackitalic: ["Poppins-BlackItalic", "sans-serif"]
        }
    },
  },
  plugins: [],
}