import {useGetTopRatedMoviesQuery, useGetMoviesListQuery} from "@/app/_redux/movies/services";
import {FlatList, Text, View} from "react-native";
import PagerView from "react-native-pager-view";
import VideoTrailer from "@/components/VideoTrailer";
import MoviePosterCard from "@/components/MoviePosterCard";
import Poster from "@/components/Poster";

const TopRatedMoviesSlider = () => {

    const { data, isLoading } = useGetTopRatedMoviesQuery({ page: 1 })
    const {results: topRatedMovies} = data || {}

    const renderItem = ({item}: {item: any}) => {
        return <Poster type="movie" key={item.id} show={item} />;
    };

    return (
        (!isLoading) ?
            <View className="w-full flex flex-col justify-center items-center">
                { topRatedMovies?.length > 0 ?
                    <FlatList
                        data={topRatedMovies}
                        renderItem={renderItem}
                        keyExtractor={item => item?.id}
                        ItemSeparatorComponent={() => <View className="w-[20px]" />}
                        ListEmptyComponent={() => <Text>Vide</Text>}
                        horizontal={true}
                        collapsable={true}
                    />
                    :
                    <Text>
                        Chargement
                    </Text>
                }
            </View>
            :
            <View className="h-[300px] min-h-[300px] w-full min-w-full bg-gray-600 animate-pulse" />
    )
}

export default TopRatedMoviesSlider