import {Text} from "react-native";

const CustomTitle = ({title} : { title: string }) => {
    return (
        <Text className="text-white text-xl font-pbold">
            { title }
        </Text>

    )
}

export default CustomTitle