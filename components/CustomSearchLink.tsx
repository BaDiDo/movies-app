import {Feather} from "@expo/vector-icons";
import {Link} from "expo-router";

const CustomSearchLink = ({ pathname, type, otherProps }) => {
    return (
        <Link
            className="p-3"
            href={
                {
                    pathname: pathname,
                    params: { type: type }
                }
            }
            {...otherProps}
        >
            <Feather name="search" size={24} color="white" />
        </Link>
    )
}

export default CustomSearchLink