import {ImageBackground} from "expo-image";
import {Link} from "expo-router";
import {Text, View} from "react-native";

const Poster = ({ show, type } : { show: any, type: string }) => {

    return (
        <Link
            className="w-screen"
            href={{ pathname: `/${type}`, params: { id: show?.id } }}
        >
            <View className="relative w-screen flex flex-col justify-between items-center">
                <ImageBackground
                    source={`https://image.tmdb.org/t/p/w600_and_h900_bestv2${show["poster_path"]}`}
                    className="h-[200px] w-full flex flex-col justify-end object-fill object-center"
                >
                    <View className="w-full flex flex-row justify-between items-center p-3 bg-[#000000a6]">
                        <Text className="text-white text-md font-pbold">
                            { type === "movie" ? show?.title : show?.name }
                        </Text>
                        <Text className="text-white font-pblack">
                            { show["vote_average"].toFixed(1) + " / 10" }
                        </Text>
                    </View>
                </ImageBackground>
            </View>
        </Link>
    )

}

export default Poster