import {useGetTopRatedTvShowsQuery} from "@/app/_redux/movies/services";
import {FlatList, Text, View} from "react-native";
import Poster from "@/components/Poster";

const TopRatedTvShowsSlider = () => {

    const { data, isLoading } = useGetTopRatedTvShowsQuery({ page: 1 })
    const {results: topRatedTvShows} = data || {}

    const renderItem = ({item}: {item: any}) => {
        return <Poster type="tv" key={item.id} show={item} />;
    };

    return (
        !isLoading ?
            <View className="flex flex-col justify-start items-center">
                { topRatedTvShows?.length > 0 ?
                    <View className="w-full flex flex-col justify-center items-center">
                        { topRatedTvShows?.length > 0 ?
                            <FlatList
                                data={topRatedTvShows}
                                renderItem={renderItem}
                                keyExtractor={item => item?.id}
                                ItemSeparatorComponent={() => <View className="w-[20px]" />}
                                ListEmptyComponent={() => <Text>Vide</Text>}
                                horizontal={true}
                                collapsable={true}
                            />
                            :
                            <Text>
                                Chargement
                            </Text>
                        }
                    </View>
                    :
                    <Text>
                        Chargement
                    </Text>
                }
            </View>
            :
            <View className="h-[300px] min-h-[300px] w-full min-w-full bg-gray-600 animate-pulse" />
    )
}

export default TopRatedTvShowsSlider