import { useState } from "react";
import { ResizeMode, Video } from "expo-av";
import { View, TouchableOpacity } from "react-native";
import {Entypo} from "@expo/vector-icons";
import {useGetMovieVideosQuery} from "@/app/_redux/movies/services";
import {Image} from "expo-image";
import images from "@/constants/Images";
import WebView from "react-native-webview";

const VideoTrailer = ({ link, thumbnail } : { link: string, thumbnail: string }) => {

    const [play, setPlay] = useState(false);

    return (
        <View className="h-[300px] w-full">
            <WebView
                className="w-full h-60 rounded-xl mt-3"
                source={{ uri: `https://www.youtube.com/embed/${link}` }}
            />
        </View>
    );
};

export default VideoTrailer;