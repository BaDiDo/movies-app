import {useSession} from "@/providers/AuthProvider";
import {Redirect, Stack} from "expo-router";
import {useSelector} from "react-redux";
import {selectToken} from "@/app/_redux/auth/authSlice";

const ProtectedStackScreen = ({...props}) => {

    const token = useSelector(selectToken)

    const {session, isLoading} = useSession();

    return ( !token ? <Stack.Screen {...props} /> : <Redirect href="/sign-up"/>)
}

export default ProtectedStackScreen