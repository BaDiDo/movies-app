import {ActivityIndicator, Text, TouchableOpacity, View} from "react-native";

const CustomSubmitButton = ({ title, handleSubmit, isSubmitting }) => {
    return (
        <TouchableOpacity
            disabled={isSubmitting}
            className="w-full min-w-full min-h-1 bg-cyan-500 bg-gradient-to-r from-cyan-500 to-blue-700 text-white rounded-xl"
            onPress={handleSubmit}
        >
            <View className="p-5 w-full flex-col justify-items-center items-center">
            { isSubmitting ?
                <ActivityIndicator animating={isSubmitting} color="#fff" />
                :
                <Text className="text-white font-pbold text-center">
                    {title}
                </Text>
            }
            </View>
        </TouchableOpacity>
    )
}

export default CustomSubmitButton