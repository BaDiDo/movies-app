import {Text, TouchableOpacity, View} from "react-native";

const PageItem = ( { params, setParams, page } ) => {

    return (
        <View>
            <TouchableOpacity
                className="bg-white mx-1 min-h-[40px] min-w-[40px] min-h-9 text-sm justify-center items-center rounded-full"
                onPress={() => { setParams({ ...params , page: page }) }}
            >
                <Text>
                    { page }
                </Text>
            </TouchableOpacity>
        </View>
    )

}
const Pagination = ({ params, setParams, pages }) => {
    return (
        <>
            <View className="w-full h-[70px] flex flex-row justify-center items-center p-3 gap-x-2">
                <View className="flex-1 flex flex-row justify-end">

                    { params?.page > 10 &&
                        <PageItem params={params} setParams={setParams} page={1} />
                    }

                    { params?.page > 2 &&
                        <PageItem params={params} setParams={setParams} page={params?.page - 2} />
                    }

                    { params?.page > 1 &&
                        <PageItem params={params} setParams={setParams} page={params?.page - 1} />
                    }


                </View>

                <View className="shrink-0">
                    <View className="min-w-4">
                        <Text className="text-white">
                            { params?.page }
                        </Text>
                    </View>
                </View>

                <View className="flex-1 flex flex-row justify-start">
                    { params?.page < pages &&
                        <PageItem params={params} setParams={setParams} page={params?.page + 1} />
                    }

                    { params?.page < pages &&
                        <PageItem params={params} setParams={setParams} page={params?.page + 2} />
                    }

                    { pages > 30 &&
                        <PageItem params={params} setParams={setParams} page={pages} />
                    }
                </View>

            </View>
        </>
    )
}

export default Pagination
