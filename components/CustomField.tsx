import {Text, TextInput, View} from "react-native";

export const FormikInput = ({placeholder, handleChange, handleBlur, value, secureTextEntry, error, ...props}) => (
    <>
        <View className={`text-white p-3 bg-[#2a2a2a] border-2 ${(error) ? "border-red-400" : "border-[#2a2a2a]"} focus:border-white rounded-xl`}>
            <TextInput
                placeholderTextColor="white"
                className="text-white font-pbold"
                placeholder={placeholder}
                onChangeText={handleChange}
                onBlur={handleBlur}
                value={value}
                secureTextEntry={secureTextEntry}
                {...props}
            />

            { error &&
                <View className="absolute bottom-0 px-3 py-1">
                    <Text className="text-red-500 text-xs first-letter:capitalize">
                        { error }
                    </Text>
                </View>
            }
        </View>
    </>
);

export default FormikInput