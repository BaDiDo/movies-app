import {Text, View} from "react-native";
import {useGetTvShowVideosQuery} from "@/app/_redux/movies/services";
import VideoTrailer from "@/components/VideoTrailer";
import PagerView from "react-native-pager-view";

const Trailers = ({id} : { id: number }) => {

    const { data, isLoading } = useGetTvShowVideosQuery(id)
    const { results: videos } = data || {}

    return (
        !isLoading ?
            <View className="flex flex-col justify-start items-center">
                { videos?.length > 0 ?
                    <PagerView collapsable={true} focusable={true} className="w-full h-[300px]" initialPage={0}>
                        { videos.map((video) => <VideoTrailer key={video.id} thumbnail={video.id} link={video.key} /> ) }
                    </PagerView>
                    :
                    <View className="w-full h-[300px] flex flex-col items-center justify-center">
                        <Text className="font-pbold text-white">
                            Page de videos disponibles
                        </Text>
                    </View>
                }
            </View>
                :
            <View className="h-[300px] min-h-[300px] w-full min-w-full bg-gray-600 animate-pulse" />
    )
}

export default Trailers